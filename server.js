var express = require('express');
const PORT = require('./config/properties').PORT;

var db = require('./config/database');

//init excpress
var app = express();

//appel de fonction database
db();

app.listen(PORT, (res, req) =>{
    console.log(" Server running on ", PORT);
})